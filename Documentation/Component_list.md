### Component list

#### Voltage level shifter

1. TLP521-2 ; x2 ; Datasheet: https://www.futurlec.com/Datasheet/LED/TLP521.pdf ; Optocoupler(8pins)
1. 1k resistor ; x4
1. 10k resistor; x4
1. 2-pin connectors; x3
1. 3-pin connectors; x2
