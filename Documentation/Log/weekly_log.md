# ITT3-PCB-GYTIS
**This log includes (as a minimum) summary of activities, lessons learned and the ressources used after each workday**  
This might be extended in case an exercise says so  
**w35**
- Summary of activities:  
1. Contacted Nordcad support for a renewed license, got an answer, waiting for the license itself.  
1. Started looking into what circuit I'm gonna make.  

**w36**
- Summary of activities:  
1. Didn't get the license yet, waiting on that.  
1. Starting on the begginer course asap when i get a response from nordcad supp.  
1. Got a reply from the support with my license! (yay)  
- Lessons learned:  
1. Deeper insight into orcad and capturecis  
- Ressources used:  
1. PSpice, pcb editor;  
1. nordcad.eu/student-forum/beginners-course  

**w37**
- Summary of activities:  
1. We had the Nordcad workshop with their support Martin, was a great experience overall.  
1. Decided on what circuit I'll make, it's a voltage level shifter.  
1. Also did the beginners-course quickly, was a good recap.
- Lessons learned:  
1. Circuit creation
1. component management
1. pcb layout
1. simulation and analysis.  
- Ressources used:  
1. The pdf's provided by the nordcad_workshop(located in gitlab folder)  
1. CaptureCis, PSpice, pcb editor  

**w38**
- Summary of activities:  
1. Met with Jalal who explained what circuit he wants us to make and gave some insight on details of the required PCB design.  
1. Started quickly reading on the components we are going to use.
1. Started on the pcb design(Ordering next week[39])
- Lessons learned:  
1. Write down every detail of the req. design
1. Gather and document datasheets well
- Ressources used:  
1. Google mostly
1. OrCAD

**w39**
- Summary of activities:  
1. Made the schematic for voltage shifter
1. Made the pcb design and layout
2. Ordering the pcb asap after a bit of clearance in the design
- Lessons learned:  
1. Wiring schematics whilst following a diagram someone else made can be confusing
- Ressources used:  
1. OrCAD
1. The nordcad workshop
1. Datasheet of TLP521

**w40**
- Summary of activities:  
1. We had a lecture on how to use KiCad
1. Made a schematic on it to have a comparison to OrCad  
1. Finishing up on my level shifter PCB design and ordering it
	1. Ultralibrarian footprints and symbols didn't work(more like i didn't set them up correctly), but Snapeda worked for my cause and i was able to finish the design finally.
- Lessons learned:  
1. Insights and use of KiCad
1. There are many different options of software to use while making schematics and pcb layouts
1. Kicad's user interface was a bit easier to use but I would prefer OrCad because it seems hard to master but potentially it could be a strong tool.
- Ressources used:  
1. KiCad https://kicad-pcb.org/
	1. Instructions and other help at KiCad turorials and documentation http://www.kicad-pcb.org/help/getting-started/
	1. Other KiCad documentation https://docs.kicad-pcb.org/

**w41**
- Summary of activities:  
1. Waiting for order confirmation, then the order itself.
1. Wrapping up documentation on gitlab.
1. Getting ready for the poster.
1. Revisioned the PCB design so it doesn't have DRC errors anymore.

**w42**
- Summary of activities:  
1. Assembled the PCB in E-lab, but ran into an issue!  
I made a mistake choosing the resistors and chose SMD resistors which I couldn't find in the component library we have in uni.
1. Also we didn't receive the tlp521-2 optocouplers so we cannot solder them to the PCB's.
- Lessons learned:  
1. Always double-check the components chosen(which I didn't do).
1. Get components ready before receiving the PCB for faster assembly.

**update**
I have managed to assemble the PCB with the SMD parts using the reflow oven and presented the poster.
