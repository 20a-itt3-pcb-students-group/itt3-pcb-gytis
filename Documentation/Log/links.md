### This file contains some of the links I used/read upon whilst working on the PCB

1. https://www.nordcad.eu/student-forum/beginners-course/ Course
1. http://blog.optimumdesign.com/through-hole-vs-surface-mount Through-hole vs SMT
1. https://learn.sparkfun.com/tutorials/pcb-basics/all PCB basics
1. https://en.wikipedia.org/wiki/Level_shifter Level shifter

***

##### Below are useful links to standards, legislation and other PCB related ressources.
Standards
1. IPC standards http://www.hytekaalborg.dk/da/ipc-standarder
1. Perfag standards http://www.perfag.dk/specifikationer/specifikationer/
1. Harmonized european standards https://ec.europa.eu/growth/single-market/european-standards/harmonised-standards_en
1. CCC standards https://www.nist.gov/standardsgov/compliance-faqs-china-compulsory-certification-ccc-marking

***
Legislation
1. ROHS https://www.rohsguide.com/

***
Other
1. Solder alloys https://en.wikipedia.org/wiki/Solder_alloys
1. PCB substrates https://www.essentracomponents.com/en-gb/news/guides/your-pcb-substrate-a-guide-to-materials
1. Reflow, wave and hand soldering https://youtu.be/saOHrw4ezGw
1. Selective soldering https://youtu.be/p-VImd2yW5s
1. Pick and place machine https://youtu.be/S8qkaTsr2_o
1. Odd shape pick and place machine https://youtu.be/EUxBMUS45Kg
1. PCB factory https://youtu.be/ljOoGyCso8s
1. PCB Soldering Factory https://youtu.be/24ehoo6RX8w
1. Thermal profiling https://youtu.be/XqOGCXw5QCw
1. Intermetallic layer https://www.indium.com/blog/intermetallics-in-soldering.php
1. Materials lifetime https://www.indium.com/blog/materials-lifetimes-a-modest-proposal.php
1. Link collection https://blogs.mentor.com/jimmartens/blog/2019/06/21/ipc-7351-and-other-pcb-design-standards/ IPC checklist http://www.ipc.org/4.0_Knowledge/4.1_Standards/PCBA-Checklist.pdf
1. NISI orcad libraries (various footprints, padstacks and schematic symbols) https://gitlab.com/npes/nisi-orcad-libraries

***
Books
1. Handbook of Electronic Assembly https://www.smta.org/store/book_detail.cfm?book_id=436