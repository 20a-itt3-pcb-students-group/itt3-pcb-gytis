# ITT3-PCB-GYTIS

* Documentation\Log contains the reports of lessons learned and resources used.
* My_nordcad_workshop includes the files from the online nordcad workshop in week37.  
* Design contains my PCB design.
* Documentation folder is for the various documentation during the proccess of making.
* Footprins folder contains the TLP521 footprint and part model downloaded from UltraLibrarian.  
UltraLibrarian footprints did not work so therefore I'm using Snapeda provided footprints for download.
* I have updated the Documentation/Images folder with populated and unpopulated PCB pictures, aswell as the pitch and poster in approriate folders.

## Circuit that I will make:

Voltage level shifter(simple image):  
![Voltage level shifter](Documentation/Images/voltage_shifter.png)  

***

The diagram of PCB I will make:
![PCB diagram](Documentation/Images/diagram.png)

***

The schematic of PCB I will make:
![PCB schematic](Documentation/Images/schematic.png)

### Brief usage

A logic level shifter is used to shift one voltage level to another which is essential for some digital chips to work.  
Let's take an example:  
If we want to upload a sketch to the esp8266 by using arduino we need to shift tx logic of arduino to 3.3v.  
As arduino logic level is 5v, it is dangerous for esp8266 and needs to be shifted to 3.3v. It may get damaged otherwise.

### My usage

I will make a PCB that converts 24V/12V to 5V for use with arduino.